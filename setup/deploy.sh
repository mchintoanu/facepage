#!/usr/bin/env bash

# Define the path to this script's directory
# This is used for achieving path independence when running this script
readonly BASE_DIR=$(dirname "${BASH_SOURCE[0]}")
readonly PROJECT_DIR="${BASE_DIR}/.."

# Include configuration variables
source ${BASE_DIR}/deploy.conf



# Define variables and functions common to more than one section in this script

readonly PROJECT_HTDOCS_PATH="${PROJECT_DIR}/htdocs"
readonly PROJECT_CGIBIN_PATH="${PROJECT_DIR}/cgi-bin"
readonly APACHE_HTDOCS_PATH="${DOCUMENT_ROOT}/facepage"
readonly APACHE_CGIBIN_PATH="${CGIBIN_ROOT}/facepage"
readonly APACHE_DATA_PATH="${DOCUMENT_ROOT}/facepage_data"

check_content_not_present() {
    local -r path=$1
    if [ -d "${path}" ]; then
	    echo "Target directory ${path} exists. Delete or rename it before"\
	    		"you perform a deploy of this project".
	    exit 1
	fi
}





# Check if the target directories already exist

check_content_not_present ${APACHE_HTDOCS_PATH}
check_content_not_present ${APACHE_CGIBIN_PATH}



# Copy project files to target directories

mkdir -p "${APACHE_HTDOCS_PATH}"
cp -R "${PROJECT_HTDOCS_PATH}/." "${APACHE_HTDOCS_PATH}"
mkdir -p "${APACHE_CGIBIN_PATH}"
cp -R "${PROJECT_CGIBIN_PATH}/." "${APACHE_CGIBIN_PATH}"



# Create data directory

mkdir -p "${APACHE_DATA_PATH}"
chown ${APACHE_USER}:${APACHE_USER} "${APACHE_DATA_PATH}"