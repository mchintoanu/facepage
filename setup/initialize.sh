#!/usr/bin/env bash

# Define the path to this script's directory
# This is used for achieving path independence when running this script
readonly BASE_DIR=$(dirname "${BASH_SOURCE[0]}")
readonly PROJECT_DIR="${BASE_DIR}/.."

# Include configuration variables
source ${BASE_DIR}/initialize.conf



# Define variables and functions common to more than one section in this script

readonly LIST_SCRIPT="${PROJECT_DIR}/cgi-bin/list.py"
readonly CREATE_SCRIPT="${PROJECT_DIR}/cgi-bin/admin/create.py"
readonly DELETE_SCRIPT="${PROJECT_DIR}/cgi-bin/admin/delete.py"

inject_text_in_file() {
    local -r label=$1
    local -r text=$2
    local -r file=$3
    sed -i "s|${label} .*|${label} ${text}|" ${file}
}





# Make relevant py scripts executable

chmod a+x ${LIST_SCRIPT}
chmod a+x ${CREATE_SCRIPT}
chmod a+x ${DELETE_SCRIPT}



# Set correct python path in relevant py scripts

sed -i "1s|.*|#!${PYTHON_PATH}|" ${LIST_SCRIPT}
sed -i "1s|.*|#!${PYTHON_PATH}|" ${CREATE_SCRIPT}
sed -i "1s|.*|#!${PYTHON_PATH}|" ${DELETE_SCRIPT}



# Generate password file for access to admin area

mkdir -p "$(dirname "${PASSWORDS_FILE_PATH}")"
echo "${ADMIN_USERNAME}:${ADMIN_PASSWORD_HASH}" > "${PASSWORDS_FILE_PATH}"



# Inject password file path in .htaccess files

inject_password_file_path() {
	local -r target_file=$1
	inject_text_in_file "AuthUserFile" \"${PASSWORDS_FILE_PATH}\" ${target_file}
}

inject_password_file_path ${PROJECT_DIR}/htdocs/admin/.htaccess
inject_password_file_path ${PROJECT_DIR}/cgi-bin/admin/.htaccess



# Inject in config.json the paths to the default style, format, and layout

readonly CONFIG_FILE="${PROJECT_DIR}/htdocs/config.json"

inject_text_in_file "\"defaultStyle\":" \"${DEFAULT_STYLE_PATH}\", ${CONFIG_FILE}

readonly FORMAT_FILENAME="${DISPLAY_CUSTOMIZATION}_format.json"
readonly FORMAT_FILE_PATH="/facepage/config/format/${FORMAT_FILENAME}"
inject_text_in_file "\"formatScript\":" \"${FORMAT_FILE_PATH}\", ${CONFIG_FILE}

readonly LAYOUT_FILENAME="${DISPLAY_CUSTOMIZATION}_layout.css"
readonly LAYOUT_FILE_PATH="/facepage/styles/layouts/${LAYOUT_FILENAME}"
inject_text_in_file "\"layoutScript\":" \"${LAYOUT_FILE_PATH}\" ${CONFIG_FILE}