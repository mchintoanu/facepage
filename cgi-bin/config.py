import os
import json


DOC_ROOT_PATH = os.environ['DOCUMENT_ROOT']
PROJECT_DOC_PATH = DOC_ROOT_PATH + "/facepage"

with open(PROJECT_DOC_PATH + "/config.json", 'r') as file:
    configuration = json.load(file)
    RECORD_FIELD_SEPARATOR = configuration['recordFieldSeparator']
    DATA_RELATIVE_PATH = configuration['paths']['data']
    DATA_ABSOLUTE_PATH = DOC_ROOT_PATH + DATA_RELATIVE_PATH
