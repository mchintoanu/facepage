#!/usr/bin/python3

import os

import config
from LightRecord import LightRecord

print("Content-type:text/plain\r\n\r\n")

for name in os.listdir(config.DATA_ABSOLUTE_PATH):
    try:
        print(str(LightRecord(str(name))) + "\r\n")
    except Exception as e:
        print(str(e))
