from pathlib import Path

from Record import Record


class HeavyRecord(Record):

    @staticmethod
    def __write_file(filename, content):
        Path(filename).parent.mkdir(parents=True, exist_ok=True)
        with open(filename, "wb") as file:
            file.write(content)

    def __init__(self, name, group, date, small_picture, large_picture):
        Record.__init__(self, name, group, date)
        self.__small_picture = small_picture
        self.__large_picture = large_picture

    def exists(self):
        return Path(self.absolute_home()).exists()

    def save(self):
        if self.exists():
            raise Exception("Record '" + self.identifier() + "' already exists and will not be overwritten.")
        HeavyRecord.__write_file(self.small_picture_url(self.absolute_home()), self.__small_picture)
        HeavyRecord.__write_file(self.large_picture_url(self.absolute_home()), self.__large_picture)
        return self.relative_home()
