from abc import ABC

import config
import shutil


class Record(ABC):

    @staticmethod
    def identifier_separator():
        return ";"

    @staticmethod
    def replacement():
        return "_"

    @staticmethod
    def encode(text):
        return text.replace(" ", Record.replacement()).replace(Record.identifier_separator(), Record.replacement())\
                .replace(config.RECORD_FIELD_SEPARATOR, Record.replacement())

    @staticmethod
    def decode(text):
        return text.replace(Record.replacement(), " ")

    def __init__(self, name, group, date):
        self.__name = name
        self.__group = group
        self.__date = date

    def name(self):
        return self.__name

    def group(self):
        return self.__group

    def date(self):
        return self.__date

    def identifier(self):
        return Record.encode(self.__name) + Record.identifier_separator() + Record.encode(self.__group)\
               + Record.identifier_separator() + Record.encode(self.__date)

    def relative_home(self):
        return config.DATA_RELATIVE_PATH + "/" + self.identifier()

    def absolute_home(self):
        return config.DOC_ROOT_PATH + self.relative_home()

    def small_picture_url(self, home):
        return self.__picture_url(home, "small")

    def large_picture_url(self, home):
        return self.__picture_url(home, "large")

    def __picture_url(self, home, label):
        return home + "/" + self.identifier() + Record.identifier_separator() + label + ".jpg"

    def delete(self):
        shutil.rmtree(self.absolute_home())
