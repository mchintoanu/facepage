#!/usr/bin/python3

import sys
sys.path.insert(1, '..')

import cgi
from Record import Record

form = cgi.FieldStorage()
name = form.getvalue('name')
group = form.getvalue('group')
date = form.getvalue('date')

print("Content-type:text/html\r\n\r\n")

record = Record(name, group, date)
try:
    record.delete()
    print("Deleted record: '" + record.identifier() + "'<br/>")
except Exception as e:
    print("Error deleting record: '" + record.identifier() + "'<br/>")
    print(str(e) + "<br/>")