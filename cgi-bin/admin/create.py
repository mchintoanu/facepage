#!/usr/bin/python3

import sys
sys.path.insert(1, '..')

import cgi
from HeavyRecord import HeavyRecord

form = cgi.FieldStorage()
name = form.getvalue('name')
group = form.getvalue('group')
date = form.getvalue('date')
small_picture = form.getvalue('small-picture')
large_picture = form.getvalue('large-picture')

print("Content-type:text/html\r\n\r\n")

record = HeavyRecord(name, group, date, small_picture, large_picture)
try:
    path = record.save()
    print("Created record: '" + record.identifier() + "' in directory '" + path + "'<br/>")
except Exception as e:
    print("Error creating record: '" + record.identifier() + "'<br/>")
    print(str(e) + "<br/>")
