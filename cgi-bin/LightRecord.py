import config
from Record import Record


class LightRecord(Record):

    def __init__(self, text):
        elements = text.split(Record.identifier_separator())
        if len(elements) != 3:
            raise Exception("Parse error for text '" + text + "'")
        Record.__init__(self, Record.decode(elements[0]), Record.decode(elements[1]), Record.decode(elements[2]))

    def __str__(self):
        return self.name() + config.RECORD_FIELD_SEPARATOR + self.group()\
               + config.RECORD_FIELD_SEPARATOR + self.date() \
               + config.RECORD_FIELD_SEPARATOR + self.small_picture_url(self.relative_home()) \
               + config.RECORD_FIELD_SEPARATOR + self.large_picture_url(self.relative_home())
