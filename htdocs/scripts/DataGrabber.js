/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


/*
 * This object is responsible for acquiring the data from the server.
 * @param {type} callback the method to be called once the data is available
 * @returns {DataGrabber} a new data grabber
 */
const DataGrabber = function(recordFieldSeparator, callback) {

    /*
     * Sends an asynchronous HTTP GET request to the server for all records, parses the response to records, and calls
     * the callback function with the result.
     */
    this.retrieve = function() {
        const successCallback = function(response) {
            parseAllLines(response.split(/\r?\n/));
        };
        const errorCallback = function(response) {
            console.log(response);
            alert("An error has occurred in function DataGrabber.retrieve. See console log for details.");
        };
        sendHttpRequest("GET", "/cgi-bin/facepage/list.py", successCallback, errorCallback);
    };

    /*
     * Parses a list of text lines to extract records from them and calls the callback function with the resulting
     * records.
     */
    const parseAllLines = function(lines) {
        const records = [];
        for (let i = 0; i < lines.length; i++) {
            if (lines[i]) {
                const record = parseLine(lines[i]);
                if (record !== null) {
                    records.push(record);
                }
            }
        }
        callback(records);
    };

    /*
     * Parses the given line and returns a record.
     * If the line can not be parsed into a record, a warning is logged to the console and the function returns null.
     */
    const parseLine = function(line) {
        const elements = line.split(recordFieldSeparator);
        let record;
        if (elements.length !== 5) {
            console.warn("Invalid record: " + line + "; expected 5 elements bot got " + elements.length);
            record = null;
        } else {
            let date = new Date(elements[2]);
            if (isNaN(date.getTime())) {
                console.warn("Invalid record: " + line + "; incorrect date syntax");
                date = null;
            }
            record = new Record(elements[0], elements[1].toUpperCase(), date, elements[3], elements[4]);
        }
        return record;
    };
};