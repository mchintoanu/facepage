/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


/*
 * This object is responsible for acquiring a directory listing from the server.
 * @param {type} directory the directory to list
 * @param {type} filenamePattern a regular expression against which to match and filter the files in the directory
 * @param {type} callback the method to be called once the data is available
 * @returns {DirectoryContentGrabber} a new Directory content grabber
 */
const DirectoryContentGrabber = function(directory, filenamePattern, callback) {

    /*
     * Sends an asynchronous HTTP GET request to the given directory and calls the callback function with the result.
     */
    this.retrieve = function() {
        const successCallback = function(response) {
            parseDirectoryListing(response);
        };
        const errorCallback = function(response) {
            console.log(response);
            alert("An error has occurred in function DirectoryContentGrabber.retrieve. See console log for details.");
        };
        sendHttpRequest("GET", directory, successCallback, errorCallback);
    };

    /*
     * Parses a directory listing HTML response to extract filenames from it and calls the callback function with the
     * list of files.
     */
    const parseDirectoryListing = function(htmlText) {
        const html = document.createElement('html');
        html.innerHTML = htmlText;
        const aTags = html.getElementsByTagName('a');
        const filenames = [];
        for (let i = 0; i < aTags.length; i++) {
            let filename = aTags[i].attributes['href'].value;
            if (filename.match(filenamePattern)) {
                if (!filename.includes("/")) {
                    filename = directory + "/" + filename;
                }
                filenames.push(filename);
            }
        }
        callback(filenames);
    };
};