/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const Record = function(name, group, date, smallPictureUrl, largePictureUrl) {

    this.name = name;
    this.group = group;
    this.date = date;
    this.smallPictureUrl = smallPictureUrl;
    this.largePictureUrl = largePictureUrl;
    
    this.formattedDate = function() {
        return date.toISOString().slice(0,10);
    };

    this.contains = function(text) {
        return contains(name, text) || contains(group, text);
    };

    this.after = function(date) {
        return this.date !== undefined && this.date !== null && this.date >= date;
    };


    const contains = function(field, text) {
        return field.toLowerCase().indexOf(text.toLowerCase()) !== -1;
    };
    
    this.toString = function() {
        return name + "; " + group + "; " + formattedDate();
    };
};