/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const ContentPanel = function(recordsByGroup, imageSize, parentContainer, eventHub, generateExtraContent) {

    const imgWidth = imageSize.width + "px";
    let records = recordsByGroup.get("");
    let comparator = new Comparator(null, null, compareBy("name", true));
    let text;
    let date;

    const generateTable = function(records) {
        const numberOfColumns = Math.floor(window.innerWidth / imageSize.width) - 2;
        const numberOfLines = Math.ceil(records.length / numberOfColumns);
        const table = document.createElement("table");
        table.id = "content-table";
        for (let line = 0; line < numberOfLines; line++) {
            const tr = table.insertRow();
            for (let column = 0; column < numberOfColumns; column++) {
                const td = tr.insertCell();
                td.className = "content-cell";
                td.width = imgWidth;

                const index = numberOfColumns * line + column;
                if (index < records.length) {
                    populateCell(td, records[index]);
                } else {
                    populateEmptyCell(td);
                }
            }
        }
        return table;
    };

    /*
     * Populates the given table cell with data from the given record.
     */
    const populateCell = function(td, record) {
        const img = document.createElement("img");
        img.src = record.smallPictureUrl;
        img.alt = "Problem loading picture; try refreshing the page";
        img.style["max-width"] = imgWidth;
        img.style["max-height"] = imageSize.height + "px";
        const pictureLink = document.createElement("a");
        pictureLink.appendChild(img);
        pictureLink.href = record.largePictureUrl;
        pictureLink.setAttribute("data-lightbox", "picture");
        pictureLink.setAttribute("data-title", record.name);
        const div = document.createElement("div");
        div.style.width = imgWidth;
        //div.style.height = thumbnailHeight;
        div.appendChild(pictureLink);
        td.appendChild(div);

        td.appendChild(document.createTextNode(record.name));
        td.appendChild(document.createElement("br"));
        const groupLink = document.createElement("a");
        groupLink.className = "content-groupLink";
        groupLink.href = "javascript:void(0);";
        groupLink.addEventListener("click", function(event) {
            eventHub.trigger(eventHub.GROUP_CHANGE, event.target.text);
            eventHub.trigger(eventHub.REFRESH);
        });
        groupLink.appendChild(document.createTextNode(record.group));
        td.appendChild(groupLink);
        if (generateExtraContent) {
            td.appendChild(document.createElement('br'));
            td.appendChild(generateExtraContent(record));
        }
    };

    /*
     * Populates the given table cell with empty content.
     */
    const populateEmptyCell = function(td) {
        const div = document.createElement("div");
        div.style.width = imgWidth;
        td.appendChild(div);
    };


    eventHub.register(eventHub.DATE_CHANGE, function(newDate) {
        /* The date may be null */
        date = newDate;
    });

    eventHub.register(eventHub.SORT_CHANGE, function(newComparator) {
        if (newComparator !== undefined && newComparator !== null) {
            comparator = newComparator;
            records.sort(comparator.operation);
        }
    });

    eventHub.register(eventHub.GROUP_CHANGE, function(newGroup) {
        if (newGroup === undefined || newGroup === null || newGroup.trim() === "" || !recordsByGroup.has(newGroup)) {
            newGroup = "";
        }
        records = recordsByGroup.get(newGroup).sort(comparator.operation);
    });

    eventHub.register(eventHub.TEXT_CHANGE, function(newText) {
        /* The text may be null */
        text = newText;
    });

    eventHub.register(eventHub.REFRESH, function() {
        let recordsFilteredByDate;
        if (date !== undefined && date !== null) {
            recordsFilteredByDate = [];
            for (let i = 0; i < records.length; i++) {
                const record = records[i];
                if (record.after(date)) {
                    recordsFilteredByDate.push(record);
                }
            }
        } else {
            recordsFilteredByDate = records;
        }
        let recordsFilteredByDateAndText;
        if (text !== undefined && text !== null && text.trim() !== "") {
            recordsFilteredByDateAndText = [];
            for (let i = 0; i < recordsFilteredByDate.length; i++) {
                const record = recordsFilteredByDate[i];
                if (record.contains(text)) {
                    recordsFilteredByDateAndText.push(record);
                }
            }
        } else {
            recordsFilteredByDateAndText = recordsFilteredByDate;
        }
        const table = generateTable(recordsFilteredByDateAndText);
        parentContainer.innerHTML = "";
        parentContainer.appendChild(table);
        parentContainer.style.width = table.offsetWidth + "px";
        scroll(0,0);
        eventHub.trigger(eventHub.COUNT_CHANGE,
                recordsFilteredByDateAndText.length + " / " + recordsByGroup.get("").length);
    });
};
