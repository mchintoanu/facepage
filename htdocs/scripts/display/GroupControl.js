/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const GroupControl = function(groups, eventHub) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = "Filter records by group";
        container.appendChild(document.createTextNode("Group: "));
        container.appendChild(activeElement);
        return container;
    };


    const activeElement = generateActiveElement();

    function generateActiveElement() {
        const selector = document.createElement("select");
        selector.className = "control-element";
        for (let i = 0; i < groups.length; i++) {
            const option = document.createElement("option");
            option.className = "control-element";
            option.value = groups[i];
            option.innerHTML = groups[i];
            selector.appendChild(option);
        }
        selector.onchange = function() {
            eventHub.trigger(eventHub.GROUP_CHANGE, selector.value);
            eventHub.trigger(eventHub.REFRESH);
        };
        return selector;
    };


    eventHub.register(eventHub.GROUP_CHANGE, function(group) {
        if (group === undefined || group === null || group.trim() === "" || !groups.includes(group)) {
            group = "";
            removeParameter("group");
        } else {
            group = group.toUpperCase();
            setParameter("group", group);
        }
        activeElement.value = group;
    });

    eventHub.trigger(eventHub.GROUP_CHANGE, getParameter("group"));
};