/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const SortControl = function(eventHub, format) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = "Change record sorting rule";
        container.appendChild(document.createTextNode("Sort by: "));
        container.appendChild(activeElement);
        return container;
    };


    const comparatorsById = new Map();

    function initComparator(identifier, name, operation) {
        comparatorsById.set(identifier, new Comparator(identifier, name, operation));
    }

    initComparator("random", "random", randomOneOrMinusOne());
    initComparator("name", "name", compareBy("name", true));
    if (format.useYear) {
        initComparator("date", format.dateSortLabel, compareBy("date", true));
        initComparator("date_reversed", format.dateReversedSortLabel, compareBy("date", false));
    }
    initComparator("group", "group", compareBy("group", true));


    const activeElement = generateActiveElement();

    function generateActiveElement() {
        const select = document.createElement("select");
        select.className = "control-element";
        comparatorsById.forEach(function(value, key, map) {
            const option = document.createElement("option");
            option.className = "control-element";
            option.value = value.identifier;
            option.innerHTML = value.name;
            select.appendChild(option);
        });
        select.onchange = function() {
            eventHub.trigger(eventHub.SORT_CHANGE, comparatorsById.get(select.value));
            eventHub.trigger(eventHub.REFRESH);
        };
        return select;
    };


    eventHub.register(eventHub.SORT_CHANGE, function(comparator) {
        //TODO watch out for that !comparatorsById.has(comparator.identifier) check
        if (comparator === undefined || comparator === null || !comparatorsById.has(comparator.identifier)) {
            /* The 'sort' is a special case; it must have a value (unlike the filters, which may be empty) */
            comparator = comparatorsById.get(activeElement.value);
            eventHub.trigger(eventHub.SORT_CHANGE, comparator);
        }
        setParameter("sort", comparator.identifier);
        activeElement.value = comparator.identifier;
    });

    eventHub.trigger(eventHub.SORT_CHANGE, comparatorsById.get(getParameter("sort")));
};