/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const DateControl = function(eventHub, format) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = format.dateHelpText;
        container.appendChild(document.createTextNode(format.dateFilterLabel));
        container.appendChild(yearActiveElement);
        if (format.useMonth) {
            container.appendChild(monthActiveElement);
        }
        return container;
    };


    const yearActiveElement = generateYearActiveElement();
    const monthActiveElement = generateMonthActiveElement();

    function generateYearActiveElement() {
        const selector = generateSelector(format.years, "0000");
        selector.onchange = function() {
            const year = selector.value;
            if (year === "") {
                monthActiveElement.disabled = true;
                monthActiveElement.value = "";
            } else {
                monthActiveElement.disabled = false;
                monthActiveElement.value = "01";
            }
            eventHub.trigger(eventHub.DATE_CHANGE, getDate());
            eventHub.trigger(eventHub.REFRESH);
        };
        return selector;
    }

    function generateMonthActiveElement() {
        const values = [];
        for (let i = 1; i <= 12; i++) {
            values.push(i);
        }
        const selector = generateSelector(values, "00");
        selector.disabled = true;
        selector.onchange = function() {
            const month = selector.value;
            if (month === "") {
                selector.value = "01";
            }
            eventHub.trigger(eventHub.DATE_CHANGE, getDate());
            eventHub.trigger(eventHub.REFRESH);
        };
        return selector;
    }

    function generateSelector(values, padding) {
        const selector = document.createElement("select");
        selector.className = "control-element";
        selector.appendChild(emptyOption());
        for (let i = 0; i < values.length; i++) {
            const value = padding ? pad(values[i], padding) : values[i];
            const option = document.createElement("option");
            option.className = "control-element";
            option.value = value;
            option.innerHTML = value;
            selector.appendChild(option);
        }
        return selector;
    }

    function emptyOption() {
        const option = document.createElement("option");
        option.className = "control-element";
        option.value = "";
        option.innerHTML = "";
        return option;
    }

    function getDate() {
        return parseDate(yearActiveElement.value + "-" + monthActiveElement.value);
    }


    eventHub.register(eventHub.DATE_CHANGE, function(date) {
        if (date === undefined || date === null) {
            yearActiveElement.value = "";
            monthActiveElement.value = "";
            monthActiveElement.disabled = true;
            removeParameter("date");
        } else {
            yearActiveElement.value = date.getFullYear();
            monthActiveElement.value = formatMonth(date);
            monthActiveElement.disabled = false;
            setParameter("date", formatDate(date));
        }
    });

    eventHub.trigger(eventHub.DATE_CHANGE, parseDate(getParameter("date")));
};