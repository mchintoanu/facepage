/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const SearchControl = function(eventHub) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = "Filter records by a given text";
        container.appendChild(document.createTextNode("Search: "));
        container.appendChild(activeElement);
        return container;
    };


    const activeElement = generateActiveElement();

    function generateActiveElement() {
        const input = document.createElement("input");
        input.className = "control-element";
        input.type = "search";
        input.oninput = function() {
            eventHub.trigger(eventHub.TEXT_CHANGE, input.value);
            eventHub.trigger(eventHub.REFRESH);
        };
        return input;
    };


    eventHub.register(eventHub.TEXT_CHANGE, function(text) {
        if (text === undefined || text === null || text.trim() === "") {
            text = "";
            removeParameter("search");
        } else {
            setParameter("search", text);
        }
        activeElement.value = text;
    });

    eventHub.trigger(eventHub.TEXT_CHANGE, getParameter("search"));
};