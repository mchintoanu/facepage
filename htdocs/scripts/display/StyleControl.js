/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const StyleControl = function(styles, styleHtmlElement, eventHub) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = "Change page style";
        container.appendChild(document.createTextNode("Style: "));
        container.appendChild(activeElement);
        return container;
    };


    const activeElement = generateActiveElement();

    function generateActiveElement() {
        const selector = document.createElement("select");
        selector.className = "control-element";
        for (let i = 0; i < styles.length; i++) {
            const option = document.createElement("option");
            option.className = "control-element";
            option.value = styles[i];
            option.innerHTML = styles[i].substring(styles[i].lastIndexOf("/") + 1, styles[i].lastIndexOf(".css"));
            selector.appendChild(option);
            if (styleHtmlElement.href.endsWith(option.value)) {
                selector.value = option.value;
            }
        }
        selector.onchange = function() {
            eventHub.trigger(eventHub.STYLE_CHANGE, selector.value);
        };
        return selector;
    };


    eventHub.register(eventHub.STYLE_CHANGE, function(style) {
        if (styles.includes(style)) {
            activeElement.value = style;
            new StyleManager(styleHtmlElement).changeStyle(style);
        }
    });

    eventHub.trigger(eventHub.STYLE_CHANGE);
};
