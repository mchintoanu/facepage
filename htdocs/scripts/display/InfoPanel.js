/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const InfoPanel = function(eventHub) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = "Number of records displayed out of the total number of records";
        container.appendChild(activeElement);
        return container;
    };


    const activeElement = document.createTextNode("");


    eventHub.register(eventHub.COUNT_CHANGE, function(text) {
        activeElement.textContent = text;
    });
};