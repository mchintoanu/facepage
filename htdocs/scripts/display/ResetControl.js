/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const ResetControl = function(eventHub) {

    this.generate = function() {
        const container = document.createElement("span");
        container.className = "control-container";
        container.title = "Reset all filters";
        container.appendChild(generateActiveElement());
        return container;
    };


    function generateActiveElement() {
        const button = document.createElement("button");
        button. innerHTML = "Reset filters";
        button.className = "control-element";
        button.onclick = function() {
            eventHub.trigger(eventHub.DATE_CHANGE, null);
            eventHub.trigger(eventHub.GROUP_CHANGE, null);
            eventHub.trigger(eventHub.TEXT_CHANGE, null);
            eventHub.trigger(eventHub.REFRESH);
        };
        return button;
    }
};