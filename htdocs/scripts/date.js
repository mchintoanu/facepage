/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


function parseDate(text) {
    let date;
    if (text !== undefined && text !== null && text.trim() !== "") {
        const match = /(\d\d\d\d)-(\d?\d)/g.exec(text);
        if (match !== null) {
            const year = match[1];
            let month = match[2];
            if (month < 1 || month > 12) {
                month = 1;
            }
            date = new Date(year, month - 1);
        }
    }
    return date;
}

function formatMonth(date) {
    let month;
    if (date !== undefined && date !== null) {
        month = (date.getMonth() + 1);
        if (month < 10) {
            month = "0" + month;
        }
    }
    return month;
}

function formatDate(date) {
    let text;
    if (date !== undefined && date !== null) {
        text = date.getFullYear() + "-" + formatMonth(date);
    }
    return text;
}