/*
 * Copyright (c) 2020, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const MainPageInitializer = function(lightbox, compareNumbers, generateExtraContent) {
    
    const eventHub = new EventHub();
    const styleLinkElement = document.getElementById("style-link");

    this.initialize = function() {
        lightbox.option({
            resizeDuration: 100,
            imageFadeDuration: 0,
            showImageNumberLabel: false
        });
        window.onresize = function() {
            eventHub.trigger(eventHub.REFRESH);
        };

        loadJsonFile("/facepage/config.json", function(config) {
            document.getElementById("layout-link").href = config.paths.layoutScript;
            const continueInitialization = function() {
                loadJsonFile(config.paths.formatScript, function(format) {
                    new DataGrabber(config.recordFieldSeparator, function(records) {
                        initContentAndControl(records, config, format);
                    }).retrieve();
                    new DirectoryContentGrabber(config.paths.styles, ".+\.css", initStyles).retrieve();
                });
            };
            new StyleManager(styleLinkElement, continueInitialization, continueInitialization).changeStyle();
        });
    };

    const initContentAndControl = function(records, config, format) {
        const recordsByGroup = new Map();
        recordsByGroup.set("", []);
        const years = new Set();
        for (let i = 0; i < records.length; i++) {
            const record = records[i];
            recordsByGroup.get("").push(record);
            const group = record.group;
            if (recordsByGroup.get(group) === undefined) {
                recordsByGroup.set(group, []);
            }
            recordsByGroup.get(group).push(record);
            years.add(record.date.getFullYear());
        }
        format.years = Array.from(years);
        format.years.sort(compareNumbers);
        new ContentPanel(recordsByGroup, config.imageSizes.small, document.getElementById("content-panel"),
                eventHub, generateExtraContent);

        const groups = new Array();
        const groupIterator = recordsByGroup.keys();
        let element = groupIterator.next();
        while (!element.done) {
            groups.push(element.value);
            element = groupIterator.next();
        }
        groups.sort();

        document.getElementById("filter-box").appendChild(new ResetControl(eventHub).generate());
        document.getElementById("filter-box").appendChild(new GroupControl(groups, eventHub).generate());
        if (format.useYear) {
            document.getElementById("filter-box").appendChild(new DateControl(eventHub, format).generate());
        }
        document.getElementById("filter-box").appendChild(new SearchControl(eventHub).generate());
        document.getElementById("info-box").appendChild(new InfoPanel(eventHub).generate());
        document.getElementById("sort-box").appendChild(new SortControl(eventHub, format).generate());

        eventHub.trigger(eventHub.REFRESH);
    };

    const initStyles = function(styleFilenames) {
        const styleControl = new StyleControl(styleFilenames, styleLinkElement, eventHub);
        document.getElementById("style-box").appendChild(styleControl.generate());
    };
};