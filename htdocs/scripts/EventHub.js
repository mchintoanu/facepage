/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const EventHub = function() {

    this.COUNT_CHANGE = new Event();
    this.DATE_CHANGE = new Event();
    this.SORT_CHANGE = new Event();
    this.STYLE_CHANGE = new Event();
    this.GROUP_CHANGE = new Event();
    this.TEXT_CHANGE = new Event();
    this.REFRESH = new Event();

    this.register = function(event, callback) {
        if (callbacksByEvent.get(event) === undefined) {
            callbacksByEvent.set(event, []);
        }
        callbacksByEvent.get(event).push(callback);
    };

    this.trigger = function(event, value) {
        callbacksByEvent.get(event).forEach(function(callback) {
            callback(value);
        });
    };


    const callbacksByEvent = new Map();
};


const Event = function() { };