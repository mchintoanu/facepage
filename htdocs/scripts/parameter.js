/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


function getParameter(name) {
    const match = RegExp("[?&]" + name + "=([^&]*)").exec(location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

function setParameter(name, value) {
    removeParameter(name);
    const currentUri = location.search;
    let parameter = encodeURIComponent(name) + "=" + encodeURIComponent(value);
    if (currentUri.indexOf("?") !== -1) {
        parameter = "&" + parameter;
    } else {
        parameter = "?" + parameter;
    }
    history.replaceState({}, "", currentUri + parameter);
}

function removeParameter(name) {
    let reduced = location.search.replace(new RegExp(name + "=[^&]*&?"), "");
    if (reduced.endsWith("&")) {
        reduced = reduced.substring(0, reduced.length - 1);
    }
    history.replaceState({}, "", reduced);
}