/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const sendHttpRequest = function(verb, target, successCallback, errorCallback, payload) {
    const request = new XMLHttpRequest();
    request.open(verb, target);
    request.onreadystatechange = function() {
        if (request.readyState === 4) {
            if (request.status === 200) {
                successCallback(request.response);
            } else {
                errorCallback(request.response);
            }
        }
    };
    request.send(payload);
};

const loadJsonFile = function(file, successCallback) {
    const onSuccess = function(response) {
        successCallback(JSON.parse(response));
    };
    const onError = function(response) {
        console.log(response);
        alert("Error loading JSON file '" + file + "'. See console log for details.");
    };
    sendHttpRequest("GET", file, onSuccess, onError);
};