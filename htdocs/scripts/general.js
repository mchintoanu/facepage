/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


function compareBy(propertyName, direct) {
    return function (object1, object2) {
        const diff = (object1[propertyName] < object2[propertyName])
                ? -1 : (object1[propertyName] > object2[propertyName]) ? 1 : 0;
        return direct ? diff : -diff;
    };
}

function randomOneOrMinusOne() {
    return function () {
        const random = Math.floor(Math.random() * 2);
        return random === 0 ? -1 : 1;
    };
}

function compareNumbers(a, b) {
    return a - b;
}

function pad(text, padding) {
    return typeof text === 'undefined' ? text : (padding + text).slice(-padding.length);
}