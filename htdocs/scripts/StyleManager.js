/*
 * Copyright (c) 2020, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const StyleManager = function(styleHtmlElement, successCallback, failureCallback) {

    const styleSorageKey = "facepage.style";

    this.changeStyle = function(style) {
        if (style) {
            setStyle(style);
        } else {
            style = localStorage.getItem(styleSorageKey);
            if (style) {
                setStyle(style);
            } else {
                loadAndSetDefaultStyle();
            }
        }
    };
    
    const setStyle = function(style) {
        styleHtmlElement.href = style;
        localStorage.setItem(styleSorageKey, style);
        if (successCallback) {
            successCallback();
        }
    };
    
    const loadAndSetDefaultStyle = function() {
        loadJsonFile("/facepage/config.json", function(config) {
            const defaultStyle = config.paths.defaultStyle;
            const onSuccess = function() {
                setStyle(defaultStyle);
            };
            const onFailure = function() {
                console.error("Missing default style file: " + defaultStyle);
                if (failureCallback) {
                    failureCallback();
                }
            };
            sendHttpRequest("HEAD", defaultStyle, onSuccess, onFailure);
        });
    };
};
