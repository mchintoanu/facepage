/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const FilenameParser = function() {
    
    /*
     * Parses the given filename and returns an employee record.
     * If the filename can not be parsed into a record, a warning is logged and the function returns
     * null.
     */
    this.parse = function(filename) {
        let record;
        const groupStartIndex = filename.lastIndexOf("(");
        const groupEndIndex = filename.lastIndexOf(")");
        if (groupStartIndex === -1 || groupEndIndex === -1) {
            record = null;
        } else {
            name = filename.substring(filename.lastIndexOf("/") + 1, groupStartIndex).trim();
            group = filename.substring(groupStartIndex + 1, groupEndIndex).trim();
            date = extractDate(filename);
            record = new Record(name, group, date);
        }
        return record;
    };

    const extractDate = function(filename) {
        let date;
        const startIndex = filename.lastIndexOf("[");
        const endIndex = filename.lastIndexOf("]");
        if (startIndex === -1 || endIndex === -1) {
            date = null;
        } else {
            date = new Date(filename.substring(startIndex + 1, endIndex));
            if (isNaN(date.getTime())) {
                date = null;
            }
        }
        return date;
    };
};
