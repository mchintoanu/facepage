/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const ImageSize = function(width, height) {

    const self = this;
    this.width = width;
    this.height = height;


    this.ratio = function() {
        return width / height;
    };

    this.longSide = function() {
        return Math.max(width, height);
    };
    
    this.isGreaterThan = function(other) {
        return width > other.width && height > other.height;
    };
    
    this.isGreaterThanOrEqualTo = function(other) {
        return width >= other.width && height >= other.height;
    };

    this.croppedProportionally = function(other) {
        validateSize(other);
        let result;
        if (self.ratio() < other.ratio()) {
            result = cutHeightProportionally(other);
        } else if (self.ratio() > other.ratio()) {
            result = cutWidthProportionally(other);
        } else {
            result = self;
        }
        return result;
    };

    const validateSize = function(other) {
        if (width < other.width || height < other.height) {
            throw "size is too low (" + self.toString() + "); should be at least " + other.toString();
        }
    };

    const cutHeightProportionally = function(other) {
        const newHeight = Math.round(width / other.ratio());
        return new ImageSize(width, newHeight);
    };

    const cutWidthProportionally = function(other) {
        const newWidth = Math.round(height * other.ratio());
        return new ImageSize(newWidth, height);
    };

    this.shrunkProportionally = function(other) {
        validateSize(other);
        let result;
        if (self.ratio() < other.ratio()) {
            result = shrinkToWidth(other.width);
        } else if (self.ratio() > other.ratio()) {
            result = shrinkToHeight(other.height);
        } else {
            result = other;
        }
        return result;
    };

    const shrinkToWidth = function(newWidth) {
        const newHeight = Math.round(newWidth / self.ratio());
        return new ImageSize(newWidth, newHeight);
    };

    const shrinkToHeight = function(newHeight) {
        const newWidth = Math.round(newHeight * self.ratio());
        return new ImageSize(newWidth, newHeight);
    };

    this.toString = function() {
        return width + "px x " + height + "px";
    };
};