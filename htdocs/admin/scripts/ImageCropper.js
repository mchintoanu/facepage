/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const ImageCropper = function(imageElement, smallImageSize, drawContainer) {
    
    const self = this;
    
    const imageSize = new ImageSize(imageElement.width, imageElement.height);
    const cropSize = imageSize.croppedProportionally(smallImageSize);
    const options = {
        viewMode: 1,
        aspectRatio: cropSize.ratio(),
        autoCropArea: 1,
        cropBoxResizable: false,
        dragMode: "move",
        toggleDragModeOnDblclick: false,
        crop() {
            draw();
        }
    };
    const cropper = new Cropper(imageElement, options);
    
    const draw = function() {
        if (drawContainer) {
            const image = new Image();
            const canvas = self.crop(image);

            const picture = document.createElement("img");
            picture.id = "result";
            picture.src = canvas.toDataURL('image/jpeg');

            drawContainer.innerHTML = '';
            drawContainer.appendChild(picture);
        }
    };

    this.crop = function(image) {
        image.src = imageElement.src;
        const canvas = document.createElement('canvas');
        const crop = cropper.getData(true);
        canvas.width = cropSize.width;
        canvas.height = cropSize.height;
        canvas.getContext('2d')
                .drawImage(image, crop.x, crop.y, crop.width, crop.height, 0, 0, canvas.width, canvas.height);
        return canvas;
    };
};