/*
 * Copyright (c) 2019, Mihai Chintoanu. All rights reserved.
 *
 * This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL),
 * Version 3, or any later version.
 * You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html
 *
 * This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
 */


const ImageLoader = function(minimumImageSize, maximumImageSize, successCallback, failureCallback) {
    
    this.load = function(file) {
        const reader = new FileReader();
        reader.onload = function() {
            const image = new Image();
            image.src = reader.result;
            image.onload = function() {
                process(image, reader.result);
            };
        };
        reader.readAsDataURL(file);
    };
    
    const process = function(image, originalUrl) {
        const imageSize = new ImageSize(image.width, image.height);
        if (imageSize.isGreaterThanOrEqualTo(minimumImageSize)) {
            let url;
            if (imageSize.isGreaterThan(maximumImageSize)) {
                const cropSize = imageSize.shrunkProportionally(maximumImageSize);
                url = new ImageResizer(cropSize).resize(image).toDataURL('image/jpeg');
            } else {
                url = originalUrl;
            }
            successCallback(url);
        } else {
            failureCallback("Selected image is too small (" + imageSize.toString() + "); should be at least "
                    + minimumImageSize.toString());
        }
    };
};