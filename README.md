`facepage` is a simple web application for displaying pictures of people along with some information about them. It can be used for example as a simple people directory in a company showing employee pictures and teams.

# Initialization

The project is mostly set up and ready to use; it requires only a small amount of initialization, as described below.

Regardless if you initialize the project manually or automatically, you have to manually copy your style files (if you have any) in `htdocs/styles/`. You may keep both your own styles and the styles shipped with the project, or you may replace the later with your own. If you choose to replace the existing styles remember to set the correct path for the default style (as described below).

## Manual initialization

Skip this subsection if you have ssh access to the hosting machine and can do the automatic initialization described in the next subsection.

1. Make the main python scripts executable and set the path to the python executable as the first line in each one. These scripts are `cgi-bin/list.py`, `cgi-bin/admin/create.py`, and `cgi-bin/admin/delete.py`.

2. Generate a password file for access to the admin area and set the path to it in the two .htaccess files `htdocs/admin/.htaccess` and `cgi-bin/admin/.htaccess`. If you don't know how to do this look up "password protection with htaccess".

3. Set the paths to the format and layout files in `htdocs/config.json`. Choose from the formats and layouts present in `htdocs/config/format/` and  `htdocs/styles/layouts/` respectively. You'll probably need the "company" flavour for both format and layout. Also set the path to the default style.

## Automatic initialization

Skip this subsection if you have already done the manual initialization described in the previous subsection.

1. Supply values for all variables defined in `setup/initialize.conf`. The comments in the file explain what each variable is.

2. Run `setup/initialize.sh`. If you have defined the path to the password file in a directory in which you don't have write access (like `/var/www/passwd/` for example) you'll need to run `setup/initialize.sh` with `sudo`.

# Deploy

After the initialization (either manual or automatic), you must deploy the project. Like initialization, the deploy can also be done manually or automatically.

## Manual deploy

Skip this subsection if you have ssh access to the hosting machine and can do the automatic deploy described in the next subsection.

Copy or move the artifacts to the correct directories, as defined in your Apache configuration, as follows:

* copy everything from `htdocs` in a subdirectory of your document root called `facepage` (depending on your Apache configuration this could be for example `/var/www/html/facepage/`);
* in your document root create a subdirectory called `facepage_data` with write access for your apache user (usually `www-data`) which will act as the data repository;
* copy everything from `cgi-bin` in a subdirectory of your cgi-bin root called `facepage` (depending on your Apache configuration this could be for example `/usr/lib/cgi-bin/facepage/`);

## Automatic deploy

Skip this subsection if you have already done the manual deploy described in the previous subsection.

1. Supply values for all variables defined in `setup/deploy.conf`.

2. Run `setup/deploy.sh` as root.

# Usage

TODO

# License

This application is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL), Version 3, or any later version. You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html

This application is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.

# Disclaimer

Although I am a software engineer I am not a web developer, and I built this application as a hobby. I applied best practices and common sense but since I am not a professional in this particular field of software development the result might have issues (of which the most dangerous in my opinion are security issues). Please be aware of this fact if you decide to use this software. And if you happen to find an issue, let me know; I am always thankful for constructive feedback.
